package com.happypeople.xq;

import java.util.concurrent.Future;

/** One basic interface of the XQ utility.
 * A ResultingXQueue is a sink of elements, for any one such object you get back a Future<R>, which in turn
 * will give you an object of type R after this XQueue has created it.
 * @param <T> The class of your payload objects.
 * @param <R> The class of the result of processing your payload
 */
public interface XQueue<T, R> {
	/** Submits an element to this XQueue.
	 * @param element
	 * @return A Future of the result.
	 */
	Future<R> submit(T element);
}
