package com.happypeople.xq;

import java.util.concurrent.ExecutorService;
import java.util.function.Function;

public interface XQBuilder {
	/** Used to create the last XQueue of a chain of XQueues.
	 * @param executor The executorService used by the created XQueue.
	 * @param handler The handler called for any one object T sent to the created XQueue.
	 * @return The created XQueue.
	 */
	<T, R> XQueue<T, R> build(ExecutorService executor, Function<T, R> handler);

	/** Used to create a XQueue which sends the result of its handler calls
	 * to a target XQueue.
	 * @param executor The executorService used by the created XQueue
	 * @param handler The handler called for any one object T sent to the created XQueue.
	 * @param target The XQueue where all the objects R sent to wich are returned by
	 * the calls to handler.
	 * @return The created XQueue.
	 */
	<T, I, R> XQueue<T, R> build(final ExecutorService executor, Function<T, I> handler, XQueue<I, R> target);
}
