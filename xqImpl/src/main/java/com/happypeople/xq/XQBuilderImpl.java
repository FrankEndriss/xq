package com.happypeople.xq;

import java.util.concurrent.ExecutorService;
import java.util.function.Function;

public class XQBuilderImpl implements XQBuilder {

	@Override
	public <T, R> XQueue<T, R> build(final ExecutorService executor, final Function<T, R> handler) {
		return new XQueueImpl<T, R>(executor, handler);
	}

	@Override
	public <T, D, R> XQueue<T, R> build(final ExecutorService executor, final Function<T, D> handler, final XQueue<D, R> target) {
		return new XQueueWithTarget<T, D, R>(executor, handler, target);
	}
}
