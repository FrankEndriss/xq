package com.happypeople.xq;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.Function;

public class XQueueWithTarget<T, I, R> implements XQueue<T, R> {

	private final ExecutorService executorService;
	private final Function<T, I> handler;
	private final XQueue<I, R> target;

	public XQueueWithTarget(final ExecutorService executorService, final Function<T, I> handler, final XQueue<I, R> target) {
		this.executorService=executorService;
		this.handler=handler;
		this.target=target;
	}

	@Override
	public Future<R> submit(final T element) {
		final Future<Future<R>> futureR=executorService.submit(() -> target.submit(handler.apply(element)));

		return new Future<R>() {

			@Override
			public boolean cancel(final boolean mayInterruptIfRunning) {
				throw new RuntimeException("not implemented");
			}

			@Override
			public boolean isCancelled() {
				try {
					return futureR.isCancelled() || futureR.get().isCancelled();
				} catch (InterruptedException | ExecutionException e) {
					// cannot happen
					e.printStackTrace();
				}
				return true;
			}

			@Override
			public boolean isDone() {
				try {
					return futureR.isDone() && futureR.get().isDone();
				} catch (InterruptedException | ExecutionException e) {
					// cannot happen
					e.printStackTrace();
				}
				return true;
			}

			@Override
			public R get() throws InterruptedException, ExecutionException {
				return futureR.get().get();
			}

			@Override
			public R get(final long timeout, final TimeUnit unit)
					throws InterruptedException, ExecutionException, TimeoutException {
				return futureR.get(timeout, unit).get(timeout, unit);
			}

		};
	}

}
