package com.happypeople.xq;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.function.Function;

public class XQueueImpl<T, R> implements XQueue<T, R> {

	private final ExecutorService executorService;
	private final Function<T, R> handler;

	public XQueueImpl(final ExecutorService executorService, final Function<T, R> handler) {
		this.executorService = executorService;
		this.handler = handler;
	}

	@Override
	public Future<R> submit(final T element) {
		return executorService.submit(() -> handler.apply(element));
	}
}
